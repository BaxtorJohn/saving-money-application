import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBnJsuh1V3oFPboJvYzVNwf9tY_ftv3k8g',
  authDomain: 'com.reactnativefirebase',
  databaseURL: 'https://your-database-name.firebaseio.com',
  projectId: 'expense-management-20268',
  storageBucket: 'expense-management-20268.appspot.com',
  messagingSenderId: '469752567874',
  appId: '1:469752567874:ios:4b310847b90f45b0acb5db',
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };    